import os

from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import QFileDialog

qtAddSource = os.path.join("View", 'add_source.ui')

Ui_AddSourceWindow, QtSecondaryClass = uic.loadUiType(qtAddSource)


class AddSourceDialog(QtGui.QDialog, Ui_AddSourceWindow):
    def __init__(self, person_list, repository_dict):
        QtGui.QDialog.__init__(self)
        Ui_AddSourceWindow.__init__(self)
        self.sourceDict = dict()
        self.fileDict = dict()
        self.setupUi(self)
        self.setFixedSize(self.size())
        self.repository_dict = repository_dict
        self.listWidget.addItems(person_list)
        self.listWidget_3.addItems(["{} = {}".format(key, value) for key, value in self.sourceDict.iteritems()])
        self.listWidget.itemDoubleClicked.connect(self.add_to_list)
        self.source_path = None

        self.tag_dict = {
            "AUTH": "Author",
            "CALN": "Contact number",
            "PUBL": "Publication information",
            "NOTE": "Comment",
            "TITL": "Source title",
            "NAME": "Name",
            "TEXT": "Actual source text",
            "_TYPE": "Source type",
            "REPO": "Repository",
            "VALUE": "Type",
            "PAGE": "Film / volume / page number",
            "QUAY": "Source quality",
            "ADDR": "Adress",
            "PHON": "Telephone / cellphone number"
        }

        self.tag_dict_list = [value for key, value in self.tag_dict.iteritems()]
        self.comboBox.addItems(["0=Unreliable Evidence",
                                "1=Questionable",
                                "2=Secondary Evidence",
                                "3=Direct/Primary Evidence",
                                "U=Undetermined"])
        self.comboBox_2.addItems(repository_dict.keys())
        self.comboBox_3.addItems(self.tag_dict_list)

        self.pushButton.clicked.connect(self.add_source_photo)
        self.pushButton_2.clicked.connect(self.add_source_video)
        self.pushButton_3.clicked.connect(self.add_source_audio)
        self.pushButton_4.clicked.connect(self.add_source_doc)

        self.pushButton_5.clicked.connect(self.save_source)
        self.pushButton_6.clicked.connect(self.add_tag)
        self.exec_()

    def add_source_photo(self):
        source_file = QFileDialog()
        source_file.setFilter("Photo file (*.png *.jpg *.bmp)")
        source_file.exec_()
        self.source_path = source_file.selectedFiles()[0]
        self.fileDict = {
            "FORM": str(self.source_path).split(".")[-1],
            "FILE": str(self.source_path),
            "_TYPE": "PHOTO"  # Should be added as "OBJE" dict
        }
        print(self.fileDict)

    def add_source_video(self):
        source_file = QFileDialog()
        source_file.setFilter("Video file (*.avi *.mkv .mp4)")
        source_file.exec_()
        self.source_path = source_file.selectedFiles()[0]
        self.fileDict = {
            "FORM": str(self.source_path).split(".")[-1],
            "FILE": str(self.source_path),
            "_TYPE": "VIDEO"  # Should be added as "OBJE" dict
        }
        print(self.fileDict)

    def add_source_audio(self):
        source_file = QFileDialog()
        source_file.setFilter("Audio file (*.mp3 *.flac *.wav)")
        source_file.exec_()
        self.source_path = source_file.selectedFiles()[0]
        self.fileDict = {
            "FORM": str(self.source_path).split(".")[-1],
            "FILE": str(self.source_path),
            "_TYPE": "AUDIO"  # Should be added as "OBJE" dict
        }
        print(self.fileDict)

    def add_source_doc(self):
        source_file = QFileDialog()
        source_file.setFilter("Doc file (*.pdf *.doc *.html)")
        source_file.exec_()
        self.source_path = source_file.selectedFiles()[0]
        self.fileDict = {
            "FORM": str(self.source_path).split(".")[-1],
            "FILE": str(self.source_path),
            "_TYPE": "DOCUMENT"  # Should be added as "OBJE" dict
        }
        print(self.fileDict)

    def add_to_list(self):
        person = self.listWidget.currentItem().text()
        if not self.listWidget_2.findItems(person, QtCore.Qt.MatchContains):
            self.listWidget_2.addItem(person)

    def add_tag(self):
        tagDescription = str(self.comboBox_3.currentText())
        tag = [tag for tag, description in self.tag_dict.items() if description == tagDescription]
        text = str(self.lineEdit_2.text())
        self.sourceDict[tag[0]] = text
        self.listWidget_3.clear()
        self.listWidget_3.addItems(
            ["{} = {}".format(self.tag_dict[key], value) for key, value in self.sourceDict.iteritems()])

    def save_source(self):
        if self.lineEdit.text():
            self.sourceDict["TITL"] = str(self.lineEdit.text())
            self.sourceDict["QUAY"] = str(self.comboBox.currentText()).split("=")[0]
            print(self.comboBox_2.currentText())
            print(self.repository_dict)
            self.sourceDict["REPO"] = self.repository_dict[str(self.comboBox_2.currentText())]
            self.sourceDict["VALUE"] = "SOUR"
            if self.fileDict:
                self.sourceDict["OBJE"] = self.fileDict
            print(self.sourceDict)
            self.close()
            # TODO: Add file from path to workspace
            # TODO: List "TAG" "VARIABLE" "DESCRIPTION"
