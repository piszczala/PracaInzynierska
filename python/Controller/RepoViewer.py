import os

from PyQt4 import QtGui, uic
from Controller.SourceViewer import SourceViewerDialog

qtRepoViewer = os.path.join('View', 'repo_viewer.ui')
Ui_RepoViewerWindow, QtSecondaryClass = uic.loadUiType(qtRepoViewer)


class RepoViewerDialog(QtGui.QDialog, Ui_RepoViewerWindow):
    def __init__(self, gq, repo_id, source_dict):
        self.source_dict = source_dict
        self.gq = gq
        self.repo_id = repo_id
        self.tag_dict = {
            "AUTH": "Author",
            "CALN": "Contact number",
            "PUBL": "Publication information",
            "NOTE": "Comment",
            "TITL": "Source title",
            "NAME": "Name",
            "TEXT": "Actual source text",
            "_TYPE": "Source type",
            "REPO": "Repository",
            "VALUE": "Type",
            "PAGE": "Film / volume / page number",
            "QUAY": "Source quality",
            "ADDR": "Adress",
            "PHON": "Telephone / cellphone number"
        }

        QtGui.QDialog.__init__(self)
        Ui_RepoViewerWindow.__init__(self)
        self.setupUi(self)
        self.setFixedSize(self.size())

        repo_dict = self.gq.get_repo_dict(self.repo_id)
        sources_with_repo = self.gq.get_sources_with_repo()
        self.label.setText(repo_dict["NAME"])
        self.listWidget.addItems(
            ["{}: {}".format(self.tag_dict[key], str(value)) for key, value in repo_dict.iteritems() if
             key != "VALUE" and key != "TITL" and key != "OBJE"])
        self.listWidget_2.addItems(
            [self.gq.dictionary[key]['TITL'] for key, value in sources_with_repo.iteritems() if value == self.repo_id])
        self.listWidget_2.itemDoubleClicked.connect(self.source_view)
        self.exec_()

    def source_view(self):
        source_id = self.source_dict[str(self.listWidget_2.currentItem().text())]
        gui = SourceViewerDialog(self.gq, source_id)
