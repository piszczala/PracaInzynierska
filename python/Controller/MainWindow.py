import os

import yaml

from PyQt4 import QtGui, uic
from PyQt4.QtCore import QString, SIGNAL
from PyQt4.QtGui import QFileDialog

from Controller.AddSource import AddSourceDialog
from Controller.SourceViewer import SourceViewerDialog
from Controller.Workspace import WorkspaceDialog
from Controller.RepoViewer import RepoViewerDialog
from Controller.AddRepo import AddRepoDialog
from Controller.PersonViewer import PersonViewerDialog

from Logic.TidyWorkspace import TidyWorkspace
from Logic.GedcomQuery import GedcomQuery
from Logic.GedFileParser import GedFileParser

qtCreatorFile = os.path.join('View', 'main.ui')
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MainWindow(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.actionUstaw_workspace.triggered.connect(self.workspace_dialog_func)
        self.actionOpcje.triggered.connect(self.load_gedcom)
        self.pushButton_3.clicked.connect(self.add_source_dialog_func)
        self.pushButton_4.clicked.connect(self.add_repo_dialog_func)
        self.listWidget.itemDoubleClicked.connect(self.repo_view)
        self.listWidget_2.itemDoubleClicked.connect(self.source_view)
        self.listWidget_3.itemDoubleClicked.connect(self.person_view)
        self.gedcom_query = None
        self.tidy_workspace = None
        self.config = None
        self.repo_dict = dict()
        self.source_dict = dict()
        self.connect(self, SIGNAL('triggered()'), self.closeEvent)
        try:
            print(self.load_workspace())
            self.update_view()
        except IOError:
            pass

    def load_workspace(self):
        with open(".yaml", 'r') as stream:
            try:
                self.config = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        print(self.config)
        self.tidy_workspace = TidyWorkspace(self.config["Workspace"])
        self.label.setText(self.config["Workspace"])
        self.gedcom_query = GedcomQuery(GedFileParser().json_to_ged(self.config["Gedcom"]))
        self.update_view()

    def save_workspace(self):
        GedFileParser().ged_to_json(self.gedcom_query.dictionary, self.tidy_workspace.workspace_path)
        config = {"Workspace": self.tidy_workspace.workspace_path,
                  "Gedcom": "{}\\result.json".format(self.tidy_workspace.workspace_path)
                  }

        with open('.yaml', 'w') as f:
            yaml.dump(config, f, default_flow_style=False)

    def closeEvent(self, event):
        self.save_workspace()
        self.deleteLater()

    def source_view(self):
        source_id = self.source_dict[str(self.listWidget_2.currentItem().text())]
        SourceViewerDialog(self.gedcom_query, source_id)

    def repo_view(self):
        repo_id = self.repo_dict[str(self.listWidget.currentItem().text())]
        RepoViewerDialog(self.gedcom_query, repo_id, self.source_dict)

    def person_view(self):
        person_name = str(self.listWidget_3.currentItem().text())
        person_id = [key for key, value in self.gedcom_query.get_all_persons().iteritems() if value == person_name][0]
        PersonViewerDialog(self.gedcom_query, person_id, self.source_dict)

    def workspace_dialog_func(self):
        gui = WorkspaceDialog()
        self.tidy_workspace = TidyWorkspace(gui.set_workspace())
        self.label.setText(gui.set_workspace())

    def add_source_dialog_func(self):
        gui = AddSourceDialog([value for key, value in self.gedcom_query.get_all_persons().iteritems()], self.repo_dict)
        src_dict = gui.sourceDict
        try:
            new_dst = self.tidy_workspace.copy_file_to_workspace(src_dict["OBJE"]["FILE"], src_dict["REPO"])
            src_dict["OBJE"]["FILE"] = new_dst
        except KeyError:
            pass
        self.gedcom_query.add_source(src_dict)
        self.update_view()

    def add_repo_dialog_func(self):
        if self.gedcom_query:
            gui = AddRepoDialog()
            self.gedcom_query.add_repo(gui.sourceDict)
            self.update_view()

    def update_view(self):
        self.source_dict = {self.gedcom_query.dictionary[y]['TITL']: y for y in self.gedcom_query.sources_list}
        self.repo_dict = {self.gedcom_query.dictionary[y]['NAME']: y for y in self.gedcom_query.repos_list}
        self.listWidget_3.clear()
        self.listWidget_3.addItems([values for key, values in self.gedcom_query.get_all_persons().iteritems()])
        self.listWidget_2.clear()
        self.listWidget_2.addItems(self.source_dict.keys())
        self.listWidget.clear()
        self.listWidget.addItems(self.repo_dict.keys())

    def load_gedcom(self):
        if self.tidy_workspace:
            gedfile = QFileDialog(directory=QString("{}\\".format(self.label.text())))
            gedfile.setFilter("Gedcom file (*.ged)")
            gedfile.exec_()
            file_path = gedfile.selectedFiles()[0]
            ged = GedFileParser().parse_file(file_path)
            self.gedcom_query = GedcomQuery(ged)
            self.update_view()

    def get_file(self):
        self.textEdit_2.setText(QFileDialog.getOpenFileName("OMG"))

    def folder(self):
        folder = QFileDialog(caption="Choose workspace folder")
        folder.setFileMode(QFileDialog.DirectoryOnly)
        folder.exec_()
        workspace_path = str(folder.selectedFiles()[0].replace('/', '\\'))
        import os

        try:
            os.makedirs(workspace_path)
        except OSError:
            if not os.path.isdir(workspace_path):
                raise
