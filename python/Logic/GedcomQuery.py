from jsonpath_rw import parse
import re


class GedcomQuery:
    def __init__(self, gedcom_dictionary):
        self.dictionary = gedcom_dictionary
        self.sources_list = self.get_sources_path()
        self.repos_list = self.get_repos_path()
        self.lastSource = self.source_last_id()
        self.lastRepo = self.repo_last_id()

    def source_last_id(self):
        source_num_re = ("^@S(.*)@")
        return max([int(re.match(source_num_re, source).group(1)) for source in self.sources_list if
                    re.match(source_num_re, source)]) + 1

    def repo_last_id(self):
        repo_num_re = ("^@REPO(.*)@")
        return max([int(re.match(repo_num_re, repo).group(1)) for repo in self.repos_list if
                    re.match(repo_num_re, repo)]) + 1

    def get_id(self, full_path):
        return str(full_path).split('.')[0]

    def get_all_persons(self):
        return {self.get_id(x.full_path): x.value.replace("/", "")
                for x in parse('$..NAME.VALUE').find(self.dictionary)}

    def get_person_info(self, person_id, ):
        return [x.value for x in parse('$.."' + person_id + ' "').find(self.dictionary)]

    def get_source_dict(self, source_id):
        return [x.value for x in parse('$.."' + source_id + '"').find(self.dictionary)][0]

    def get_repo_dict(self, source_id):
        return [x.value for x in parse('$.."' + source_id + '"').find(self.dictionary)][0]

    def get_sources_with_repo(self):
        return {self.get_id(x.full_path):x.value for x in parse('$..REPO').find(self.dictionary)}

    def get_sources_path(self):
        return [str(x.context.path) for x in parse('$..VALUE').find(self.dictionary) if x.value == "SOUR"]

    def get_repos_path(self):
        return [str(x.context.path) for x in parse('$..VALUE').find(self.dictionary) if x.value == "REPO"]

    def get_family_path(self):
        return [str(x.context.path) for x in parse('$..VALUE').find(self.dictionary) if x.value == "FAM"]

    def add_source(self, source_dict):
        tag = "@S{}@".format(self.lastSource)
        self.lastSource += 1
        self.sources_list.append(tag)
        self.dictionary[tag] = source_dict
        return tag

    def add_source_to_event_re(self, person_id, tag, source_id, source_dict=None):
        if source_dict is None:
            source_dict = {}
        working_dict = self.dictionary["@" + person_id + "@"][tag]
        if working_dict is not dict:
            working_dict = {"VALUE": working_dict}
        if source_dict:
            working_dict["SOUR"] = source_dict
            working_dict["SOUR"]["VALUE"] = source_id
        else:
            working_dict["SOUR"] = source_id
        print(working_dict)

    def add_repo(self, repo_dict):
        tag = "@REPO{}@".format(self.lastRepo)
        self.lastRepo += 1
        self.repos_list.append(tag)
        self.dictionary[tag] = repo_dict
